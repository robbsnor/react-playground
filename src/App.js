import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import NavBar from "./components/Navbar";

import Home from "./home";
import Beers from "./beers";
import Beer from "./beer";
import NoMatch from "./404";

class App extends Component {
  render() {
    return (
      <>
        <Router>
          <NavBar></NavBar>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/beers" component={Beers} />
            <Route path="/beers/:id" component={Beer} />
            <Route component={NoMatch} />
          </Switch>
        </Router>
      </>
    );
  }
}

export default App;
