import React, { Component } from "react";
import BeerTumbnail from "./components/beerThumbnail";
import Hero from "./components/hero.js";
import SortBeers from "./components/sortBeers.js";

class Beers extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <Hero background="https://wallpaperaccess.com/full/1105642.jpg"></Hero>
        <div className="container">
          <div className="row">
            <div className="col my-4">
              <h1>Beers</h1>
              <SortBeers />
            </div>
          </div>
          <div className="row">
            <BeerTumbnail />
          </div>
        </div>
      </>
    );
  }
}

export default Beers;
