import React, { Component } from "react";
import Hero from "./components/hero";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <>
        <Hero background="https://i.pinimg.com/originals/96/78/5a/96785a4f7affaadeeeda0ac6adc43eff.jpg"></Hero>
        <div className="container">
          <div className="row">
            <div className="col my-5">
              <h1>Home</h1>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Home;
