import React, { Component } from "react";
import { Link } from "react-router-dom";
import Hero from "./components/hero.js";

class Beer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      beerIndex: null,
      beerInfo: {},
      items: [],
    };
  }

  componentDidMount() {
    fetch(`https://api.punkapi.com/v2/beers`)
      .then((res) => res.json())
      .then((response) => {
        this.setState({
          items: response,
        });
        console.log("fetched data");
        getBeerIndex();
        getBeerInfo();
      });

    const getBeerIndex = () => {
      for (var i = 0; i < this.state.items.length; i++) {
        if (this.state.items[i].name.toLowerCase() === this.props.match.params.id) {
          this.setState({
            beerIndex: i,
          });
        }
      }
    };

    const getBeerInfo = () => {
      this.setState({
        beerInfo: this.state.items[this.state.beerIndex],
      });
    };
  }

  render() {
    const { name, description, first_brewed, image_url } = this.state.beerInfo;
    return (
      <>
        <Hero background="https://external-preview.redd.it/b_9XwB81LGUCJrne2ZBQMCWYcQJNtfZ9asMexDgY1ec.jpg?auto=webp&s=ec76d0cf335211e41e6cc80b73ddaf5291cf9a03"></Hero>

        <div className="container">
          <div className="row">
            <div className="col-12 my-5">
              <Link to="/beers" className="btn btn-outline-primary">
                back
              </Link>
            </div>
          </div>

          <div className="row">
            <div className="col-12 col-md-4 my-5">
              <img src={image_url} alt="" />
            </div>
            <div className="col">
              <h1>{name}</h1>
              <p>{description}</p>
              <p>First brewed: {first_brewed}</p>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Beer;
