import React, { Component } from "react";
import { Link } from "react-router-dom";

class NoMatch extends Component {
  render() {
    document.title = "404";
    return (
      <>
        <div className="container">
          <div className="row">
            <div className="col">
              <h2>Page not found</h2>
              <p>
                go back to <Link to="/">home</Link>
              </p>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default NoMatch;
