import React from "react";
import { Link } from "react-router-dom";

import FetchBeers from "./fetchBeers";

const BeerThumbnail = () => (
  <FetchBeers>
    {({ data }) => {
      return (
        <>
          {data.map((item) => (
            <div className="col-12 col-md-4" key={item.name}>
              <div className="card mb-4">
                <div className="card-body">
                  <h5 className="card-title">{item.name}</h5>
                  <p className="card-text">{item.description.substring(0, 100)}...</p>
                  <Link to={`/beers/${item.name.toLowerCase()}`} className="btn btn-primary stretched-link">
                    More info
                  </Link>
                </div>
              </div>
            </div>
          ))}
        </>
      );
    }}
  </FetchBeers>
);

export default BeerThumbnail;

// class BeerTumbnail extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       items: [],
//     };
//   }
//

//   componentDidMount() {
//     fetch(`https://api.punkapi.com/v2/beers`)
//       .then((res) => res.json())
//       .then((response) => {
//         this.setState({
//           items: response,
//         });
//       });
//   }
//   render() {
//     return (
//       <>
//         {this.state.items.map((item) => (
//           <div className="col-12 col-md-4" key={item.name}>
//             <div className="card mb-4">
//               <div className="card-body">
//                 <h5 className="card-title">{item.name}</h5>
//                 <p className="card-text">{item.description.substring(0, 100)}...</p>
//                 <Link to={`/beers/${item.name.toLowerCase()}`} className="btn btn-primary stretched-link">
//                   More info
//                 </Link>
//               </div>
//             </div>
//           </div>
//         ))}
//       </>
//     );
//   }
// }

// export default BeerTumbnail;
