import React, { Component } from "react";
import "./Hero.scss";

class Hero extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return <div className="hero" style={{ backgroundImage: `url(${this.props.background})` }}></div>;
  }
}

export default Hero;
