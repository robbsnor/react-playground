import { Component } from "react";

class FetchBeers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentDidMount() {
    fetch(`https://api.punkapi.com/v2/beers`)
      .then((res) => res.json())
      .then((response) => {
        this.setState({
          data: response,
        });
        console.log("fetched beers");
      });
  }
  render() {
    return this.props.children(this.state);
  }
}

export default FetchBeers;
