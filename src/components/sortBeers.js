import React, { Component } from "react";

class SortBeers extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  sortyByID = () => {
    console.log("sorted by ID");
  };

  sortyAlphabetical = () => {
    console.log("sorted alphabetical");
  };

  render() {
    return (
      <>
        <button onClick={this.sortyByID} type="button" className="btn btn-outline-dark mr-2">
          ID
        </button>
        <button onClick={this.sortyAlphabetical} type="button" className="btn btn-outline-dark">
          A > Z
        </button>
      </>
    );
  }
}

export default SortBeers;
