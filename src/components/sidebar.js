import React, { Component } from "react";
import { Link } from "react-router-dom";

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }

  increment() {
    this.setState({
      count: this.state.count + 1,
    });
  }

  render() {
    return (
      <>
        <div className="sidebar">
          <div className="sidebar-menu">
            <Link to="/" className="sidebar-brand">
              Brand
            </Link>

            <div className="sidebar-content">
              <div className="mt-10 font-size-12">
                Press <kbd>/</kbd> to focus
              </div>
            </div>

            <h5 className="sidebar-title">Getting started</h5>
            <div className="sidebar-divider"></div>
            <Link to="/" className="sidebar-link active">
              Installation
            </Link>
            <Link to="/" className="sidebar-link">
              CLI commands
            </Link>
            <br />
            <h5 className="sidebar-title">Components</h5>
            <div className="sidebar-divider"></div>
            <Link to="/" className="sidebar-link">
              File explorer
            </Link>
            <Link to="/" className="sidebar-link">
              Spreadsheet
            </Link>
            <Link to="/" className="sidebar-link">
              Map
            </Link>
            <Link to="/" className="sidebar-link">
              Messenger
            </Link>
          </div>
        </div>
      </>
    );
  }
}

export default SideBar;
